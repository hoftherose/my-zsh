alias youtube-dl-audio="youtube-dl -f 140"

function clip() {
	ffmpeg -i $1 output
}

function clip_video() {
    ffmpeg -ss $2 -i $1 -c copy -t $3 clipped_$1
}