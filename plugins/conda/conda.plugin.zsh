#####################################################
# conda plugin for oh-my-zsh                        #
# Author: holyfiddlex (github.com/holyfiddlex)      #
#####################################################

alias ca='conda activate '
alias cda='conda deactivate'

alias cc='conda create -n '
alias ccf='conda env create -f '
function ccl () {
    conda create --name $2 --clone $1
}

alias ci='conda install '
alias cic='conda install -c conda-forge '

alias crm='conda env remove -n '
alias cca='conda clean -a'

alias jp='jupyter notebook --ip 127.0.0.1'
alias jpl='jupyter lab --ip 127.0.0.1'
