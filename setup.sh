which curl &> /dev/null || echo "Please install curl to continue"
which git &> /dev/null || echo "Please install git to continue"

if ((echo $0 -eq "zsh")); then
    echo "Remember to set the dir to .oh-my-zsh or the rest of the script won't work"

    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
else
    echo "ZSH installation found"
fi

echo "Moving previous custom folder"
mv ~/.oh-my-zsh/custom ~/.oh-my-zsh/custom.bak
ln -s $(pwd) ~/.oh-my-zsh/custom
git clone https://github.com/zsh-users/zsh-autosuggestions $(pwd)/plugins/zsh-autosuggestions

